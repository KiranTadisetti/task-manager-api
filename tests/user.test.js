const request = require('supertest');

const app = require('../src/app');
const User = require('../src/models/user');
const {userOne,userOneId,setUpDatabase} =require('./fixtures/db')

beforeEach(setUpDatabase);

test('Should signup a new user', async () => {
    const response =await request(app)
        .post('/users')
        .send({
            email: 'tdvkiran@live.com',
            name: 'kiran',
            password: 'testingpass'
        })
        .expect(201)
    
    const user= await User.findById(response.body.user._id);
    expect(user).not.toBeNull();
    expect(user.name).toBe('kiran')

    expect(response.body).toMatchObject({
        user:{
            email: 'tdvkiran@live.com',
            name: 'kiran'
        },
        token:user.tokens[0].token
    })

})
test('Should login a existing user', async () => {
    const response=await request(app)
        .post('/users/login')
        .send({
            email: userOne.email,
            password: userOne.password
        })
        .expect(200)

    const user= await User.findById(response.body.user._id);
    expect(response.body).toMatchObject({
        token:user.tokens[1].token
    })
})
test('Should not login a non-existing user', async () => {
    await request(app)
        .post('/users/login')
        .send({
            email: userOne.email,
            password: userOne.password+"asd"
        })
        .expect(400)
})
test('Should GET Profile of the user', async () => {
    await request(app)
        .get('/users/me')
        .set("Authorization",`Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})
test('Should not GET Profile of the unauthenticated user', async () => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401)
})

test('should Delete profile of the user', async ()=>{
    await request(app)
            .delete('/users/me')
            .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
            .send()
            .expect(200);
})
test('should not Delete profile of the user', async ()=>{
    await request(app)
            .delete('/users/me')
            .send()
            .expect(401);
})

test('should updalod avatar image', async ()=>{
    await request(app)
            .post('/users/me/avatar')
            .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
            .attach('avatar','tests/fixtures/profile-pic.jpg')
            //.send()
            .expect(200)

        const user =await User.findById(userOneId);
        expect(user.avatar).toEqual(expect.any(Buffer))
})
