const request = require('supertest');

const app = require('../src/app');
const Task = require('../src/models/task');
const {userOne,userOneId,setUpDatabase} =require('./fixtures/db')



beforeEach(setUpDatabase);

test('Should create a task for user', async ()=>{
    const response= await request(app)
    .post('/tasks')
    .set("Authorization",`Bearer ${userOne.tokens[0].token}`)
    .send({
        description:'From my test'
    })
    .expect(201)
    const task =await Task.findById(response.body._id);
    expect(task).not.toBeNull();
    expect(task.completed).toEqual(false);
})
test('Should GET all task for a user', async ()=>{
    const response= await request(app)
    .get('/tasks')
    .set("Authorization",`Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(200)
    const task =await Task.find({owner:userOne._id});
    expect(task.length).toEqual(2);
    //expect(task.completed).toEqual(false);
})