
const jwt= require('jsonwebtoken');
const mongoose=require('mongoose');

const User = require('../../src/models/user');
const Task = require('../../src/models/task');


const userOneId=new mongoose.Types.ObjectId();
const userOne = {
    _id:userOneId,
    email: 'kiran@example.com',
    name: 'tester',
    password: 'testerpass',
    tokens:[
        {
            token:jwt.sign({_id:userOneId},process.env.JWT_SECRET)
        }
    ]
}
const userTwoId=new mongoose.Types.ObjectId();
const userTwo = {
    _id:userTwoId,
    email: 'kiran123@example.com',
    name: 'tester',
    password: 'testerpass123',
    tokens:[
        {
            token:jwt.sign({_id:userTwoId},process.env.JWT_SECRET)
        }
    ]
}

const taskOne={
    _id: new mongoose.Types.ObjectId(),
    description:'First Task',
    completed:false,
    owner: userOneId
}
const taskTwo={
    _id: new mongoose.Types.ObjectId(),
    description:'second Task',
    completed:true,
    owner: userOneId
}
const taskThree={
    _id: new mongoose.Types.ObjectId(),
    description:'third Task',
    completed:true,
    owner: userTwoId
}

const setUpDatabase= async ()=>{
    await User.deleteMany();
    await Task.deleteMany();
    await new User(userOne).save();
    await new User(userTwo).save();
    await new Task(taskOne).save();
    await new Task(taskTwo).save();
    await new Task(taskThree).save();
}

module.exports ={
    userOneId,
    setUpDatabase,
    userOne,
    userTwo,
    userTwoId,
    taskOne,
    taskTwo,
    taskThree
}