const sgMail=require('@sendgrid/mail');



sgMail.setApiKey(process.env.SENDGRID_API_KEY);

//console.log(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail=(email,name)=>{

    const msg = {
        to: email,
        from: 'founder@kirantadisetti.xyz',
        subject: 'Welcome to the App',
        text: `welcome ${name}. Let me know how you get along with the app`
      };
      sgMail.send(msg).catch(e=>{
          console.log(e);
         return e;
      });
}

const GoodByeEmail=(email,name)=>{

    const msg = {
        to: email,
        from: 'founder@kirantadisetti.xyz',
        subject: 'Sorry to see you go',
        text: `Hi ${name}. I hope to see you soon.`
      };
      sgMail.send(msg).catch(e=>{
         return e;
      });
}

module.exports={
    sendWelcomeEmail,
    GoodByeEmail
}