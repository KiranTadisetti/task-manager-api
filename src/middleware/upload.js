const multer =require('multer');

const avatar= multer({
    limits:{
        fileSize:1000000
    },
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(jpg|png|jpeg)$/)){
            return cb(new Error('please upload image'));
        }
        cb(undefined,true);
    }
});

module.exports=avatar;