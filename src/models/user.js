const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Task = require('./task');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    age: {
        type: Number,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new Error('Age must be positive number');
            }
        }
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Email is invalid");
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        validate(value) {
            if (value.includes('password')) {
                throw new Error('password cannot contain password');
            }
        },
        minlength: 7
    },
    tokens: [{
        token: {
            type: String,
            required:true
        }
    }],
    avatar:{
        type:Buffer
    }

},/*Schemaoptions*/{
    timestamps:true
});


//virtual property -> a relation between two collections
userSchema.virtual('tasks',{
    ref:'Task',
    localField: '_id',
    foreignField: 'owner'
})

//statics are accessiable on model (as static method)
userSchema.statics.findByCredential = async (email, password) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        throw new Error('user not registered');
    }

    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) {
        throw new Error('password incorrect');
    }

    return user;
}

//methods are accessiable on model instance
userSchema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, 'kiranNodeJs');
    
    user.tokens=user.tokens.concat({token});
    await user.save();
    
    return token;
}

userSchema.methods.toJSON= function(){
    const user=this;
    const userObject=user.toObject();

    delete userObject.password;
    delete userObject.tokens;
    delete userObject.avatar;
    return userObject;
}

//shouldnot use arrow funtion for need "this" inside the func
userSchema.pre('save', async function (next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }

    next();
})

// Delete user tasks when user is removed
userSchema.pre('remove', async function(next){
    const user= this;

    await Task.deleteMany({owner: user._id});

    next();
})

const User = mongoose.model('User', userSchema)


module.exports = User;