const express = require('express');

const Task = require('../models/task');
const auth = require('../middleware/auth');

const router = new express.Router();


router.post('/tasks', auth, async (req, res) => {

    const task = new Task({
        ...req.body,
        owner: req.user
    })
    try {
        await task.save();
        res.status(201).send(task);
    }
    catch (err) {
        res.status(500).send(err);
    }

})
// GET /tasks?completed=true/false
//GET /tasks?limit=5&skip=0 
//skip will be useful to skip the number of records & limit is used for how many records need to be shown
//GET /tasks?sortBy=createdAt_asc/createdAt_desc
router.get('/tasks', auth, async (req, res) => {
    const match = {}

    //adding completed to match
    req.query.completed ? match.completed = req.query.completed === 'true' : null;

    //sort order
    const sort={}
    if(req.query.sortBy){
        const value=req.query.sortBy;
        const sortKeyValue=value.split('_');
        sort[sortKeyValue[0]]=sortKeyValue[1]==='asc'?1:-1;
        
    }
    try {
        //const tasks = await Task.find({owner:req.user._id});
        //alternative
        await req.user.populate({
            path: 'tasks',
            match,
            //pagination
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        }).execPopulate()

        res.send(req.user.tasks);
    }
    catch (err) {
        res.status(500).send(err);
    }

})
router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id;

    try {
        const task = await Task.findOne({ _id, owner: req.user._id });
        if (!task) {
            return res.status(400).send('No tasks by you')
        }
        res.send(task);
    }
    catch (err) {
        res.status(500).send(err);
    }

})

router.patch('/tasks/:id', auth, async (req, res) => {
    const allowedUpdates = ['description', 'completed'];
    const updates = Object.keys(req.body);
    const isValidUpdate = updates.every(update => {
        return allowedUpdates.includes(update);
    })
    if (!isValidUpdate) {
        return res.status(400).send({ error: 'Invalid Update!!' })
    }
    try {

        const task = await Task.findOne({ _id: req.params.id, owner: req.user._id });
        //const task = await Task.findById(req.params.id);

        updates.forEach(update => {
            task[update] = req.body[update];
        })

        task.save()

        if (!task) {
            return res.status(404).send({ error: 'Not Found!!' });
        }

        res.send(task);
    }
    catch (err) {
        if (err.message) {
            return res.status(400).send(err);
        }
        res.status(500).send(err);
    }
})

router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({ _id: req.params.id, owner: req.user._id });


        if (!task) {
            return res.status(404).send({ error: 'Not Found!!' });
        }
        //await task.remove();
        res.send(task);
    }
    catch (err) {
        res.status(400).send(err);
    }
})

module.exports = router;