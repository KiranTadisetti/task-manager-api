const express = require('express');
const sharp = require('sharp');

const User = require('../models/user');

const auth = require('../middleware/auth');
const upload = require('../middleware/upload');
const { sendWelcomeEmail,GoodByeEmail } = require('../emails/account');

const router = new express.Router();


router.post('/users', async (req, res) => {
    const user = new User(req.body);

    try {
        const token = await user.generateAuthToken();
        sendWelcomeEmail(user.email, user.name)
        res.status(201).send({ user, token });
    }
    catch (err) {
        res.status(400).send(err);
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredential(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    }
    catch (err) {
        res.status(400).send(err);
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter(token => {
            return token.token !== req.token; //removing particular
        })

        await req.user.save();

        res.send('you are logged out');
    } catch (e) {
        res.status(500).send('error in logging out');
    }

})
router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = [];

        await req.user.save();

        res.send('you are logged out on all devices');
    } catch (e) {
        res.status(500).send('error in logging out');
    }

})


router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
})


//patch is used for update operations
router.patch('/users/me', auth, async (req, res) => {
    const allowedUpdates = ['name', 'age', 'email', 'password'];
    const updates = Object.keys(req.body);
    const isValidUpdate = updates.every(update => {
        return allowedUpdates.includes(update);
    })

    if (!isValidUpdate) {
        return res.status(400).send({ error: 'invalid updates' })//throw new Error("connot update!! please update inside schema only");
    }

    try {
        updates.forEach((update) => {
            req.user[update] = req.body[update];
        })
        await req.user.save();

        res.send(req.user);
    }
    catch (err) {
        res.status(400).send(err);
    }
})

router.delete('/users/me', auth, async (req, res) => {
    try {

        await req.user.remove();
        GoodByeEmail(req.user.email,req.user.name);

        res.send(req.user);
    }
    catch (err) {
        res.status(400).send(err);
    }
})

router.post('/users/me/avatar', auth, upload.single('avatar'), async (req, res) => {
    try {
        //req.file.buffer data is available only when the dest is not defined on multer
        const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer();
        req.user.avatar = buffer;
        await req.user.save();
        res.send({ success: 'Avatar Uploaded!!' });
    } catch (e) {
        res.status(500).send({ error: e.message })
    }
}, (error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

router.delete('/users/me/avatar', auth, async (req, res) => {
    try {
        if (req.user.avatar) {
            req.user.avatar = undefined;
            await req.user.save();
            return res.send({ success: 'avatar deleted' });
        }
        res.send({ success: 'No avatar' })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }
});

router.get('/users/:id/avatar', async (req, res) => {
    try {
        const user = await User.findById(req.params.id);

        if (!user || !user.avatar) {
            throw new Error();
        }
        res.set('Content-Type', 'image/png').send(user.avatar);
    } catch (error) {
        res.status(404).send();
    }
});

module.exports = router;